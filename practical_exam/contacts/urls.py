from django.conf.urls import url

from contacts import views


urlpatterns = [
    url(r'^contactus/$', views.contact_view),
]