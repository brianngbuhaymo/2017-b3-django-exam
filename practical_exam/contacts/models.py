from __future__ import unicode_literals

from django.db import models

# Create your models here.
class ContactMessage(models.Model):
    email = models.EmailField(verbose_name = 'E-mail')
    subject = models.CharField(max_length=50)
    message = models.TextField(max_length=255)

    def __unicode__(self):
        return self.subject