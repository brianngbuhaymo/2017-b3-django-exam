from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from .forms import ContactMessageForm

# Create your views here.
class ContactUsView(View):
	template_name = 'contactus.html'
	form = ContactMessageForm

	def get(self, request, *args, **kwargs):
		form = self.form()
		return render(request, self.template_name, {'form': form})

	def post(self, request, *args, **kwargs):
		form = self.form(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponse('Sent!')

		return render(request,self.template_name,{'form': form})


contact_view = ContactUsView.as_view()	
