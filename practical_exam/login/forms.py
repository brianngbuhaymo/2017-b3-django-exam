from django import forms
from django.db.models import Q
from django.contrib.auth.models import User
#rom .models import 

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput())


class SignupForm(forms.ModelForm):
    class Meta:
            model = User
            fields = ['first_name', 'last_name', 'email', 'username', 'password']


    