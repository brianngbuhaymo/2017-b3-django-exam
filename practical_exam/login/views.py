from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View
from .forms import LoginForm, SignupForm
from django.contrib.auth import authenticate, login

# Create your views here.
class LoginView(View):
	template_name = 'login.html'
	form = LoginForm

	def get(self, request, *args, **kwargs):
		form = self.form()
		return render(request, self.template_name, {'form': form})

	def post(self, request, *args, **kwargs):
		form = self.form(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				return HttpResponse('Logged in.')

		return render(request,self.template_name,{'form': form})


class Signup(View):
	template_name = 'signup.html'
	form = SignupForm

	def get(self, request, *args, **kwargs):
		form = self.form()
		return render(request, self.template_name, {'form': form})

	def post(self, request, *args, **kwargs):
		form = self.form(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponse('Success!')

		return render(request,self.template_name,{'form': form})


login_view = LoginView.as_view()
signup_view = Signup.as_view()	