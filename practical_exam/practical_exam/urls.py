from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from django.contrib import admin
from contacts import views
from login import views as kani

urlpatterns = [
	url(r"^contactus/$", views.contact_view, name='contactus'),
	url(r"^login/$", kani.login_view, name='login'),
	url(r"^signup/$", kani.signup_view, name='signup'),
    url(r"^admin/", include(admin.site.urls)),
    url(r"", include("blogs.urls")),
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
